/**
 * EliteTorrent Downloader
 */

var express = require('express');
var httpinvoke = require('httpinvoke');
var cheerio = require('cheerio');
var config = require('../config/config');

exports.search = function (term, cb) {
    httpinvoke(config.ET_HOST_URL + config.ET_SEARCH_URL + term, 'GET', function (err, body, statusCode, headers) {
        if (err) {
            cb(err);
        }

        $cheerio = cheerio.load(body);
        if (!$cheerio('.mensaje.error').length) {
            var total = $cheerio('.nav h3');
            var totalShows = total[0].children[0].data.substring(
                total[0].children[0].data.indexOf("(") + 1,
                total[0].children[0].data.indexOf(")")
            );
            totalShows = totalShows.substr(6);
            cb({ total: +totalShows, pages: Math.round(totalShows / config.ET_RESULTS_PER_PAGE) + 1 });
        } else {
            cb({ total: 0, pages: 0, message: "No results searching: " + term.replace(/\+/g, " ") });
        }
    });
}