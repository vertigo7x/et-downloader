/**
 * EliteTorrent Downloader
 */

var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var cors = require('cors');
var expressValidator = require('express-validator');

var app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(expressValidator([]));

app.use(cors());
app.options('*', cors())

// Basic AUTH
// If your planning to open the service to the Internet, change the AUTH_KEY
// to your own info.
// Username: et-downloader
// Password: 12345
// More info: https://www.getpostman.com/docs/helpers
// TODO: const AUTH_KEY = "Basic ZXQtZG93bmxvYWRlcjoxMjM0NQ==";

var index = require('./routes/index');
var search = require('./routes/search');

app.use('/', index);
app.use('/search/', search);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.json({ error: err.status });
});


module.exports = app;