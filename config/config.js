/**
 * EliteTorrent Downloader
 * 
 * Constants
 */

var express = require('express');

var config = {
     ET_HOST_URL: "http://www.elitetorrent.net",
     ET_SEARCH_URL: "/resultados/",
     ET_SHOW_LINK: "a.nombre",
     ET_SHOW_MAGNET: "",
     ET_NO_RESULTS: "",
     ET_NEXT_PAGE_LINK: ".pagina.pag_sig",
     ET_RESULTS_PER_PAGE: 48
}

module.exports = config;