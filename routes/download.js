/**
 * EliteTorrent Downloader
 */

var express = require('express');
var router = express.Router();
var config = require('../config/config');

var controller = require('../controllers/download');

/**
 * Entrypoint: /download/@term
 * @param string Shown name. Normally a movie or show name.
 * @return JSON Object with magnet links of the series
 */
router.get('/:term', function (req, res, next) {

    req.checkParams('term', 'Show name parameter required').notEmpty();

    var term = req.params.term;

    console.log("Performing search of: " + term);
    console.log("On: " + config.ET_HOST_URL + config.ET_SEARCH_URL + term);

    req.getValidationResult().then(function (result) {
        if (!result.isEmpty()) {
            res.status(400).send('Missing parameter: ' + util.inspect(result.array()));
            return;
        }
        controller.search(term, function(result) { return res.json(result) });
    });    
});

module.exports = router;