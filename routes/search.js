/**
 * EliteTorrent Downloader
 */

var express = require('express');
var router = express.Router();
var config = require('../config/config');

var controller = require('../controllers/search');

/**
 * Entrypoint: /search/@term
 * @param string Search term. Normally a movie or show name. Replace spaces by plus (+) sings
 * @return JSON Object with the number of episodes/links found
 */
router.get('/:term', function (req, res, next) {

    req.checkParams('term', 'Search parameter required').notEmpty();

    var term = req.params.term;

    console.log("Performing search of: " + term);
    console.log("On: " + config.ET_HOST_URL + config.ET_SEARCH_URL + term);

    req.getValidationResult().then(function (result) {
        if (!result.isEmpty()) {
            res.status(400).send('Missing parameter: ' + util.inspect(result.array()));
            return;
        }
        controller.search(term, function(result) { return res.json(result) });
    });    
});

module.exports = router;